--=============HOMEWORK================
create table books(
    ID serial primary key,
    Title text,
    PublishDate date,
    Price real,
    AuthorID bigint not null,
    BestSeller bool
);

create table author(
    ID serial primary key,
    RealID bigint unique not null,
    BirthDate date,
    HomeCountryID bigint not null
);

create table country(
    ID serial primary key,
    Code text unique,
    Name text unique
);

alter table books
add foreign key (AuthorID) references author(ID);
alter table author
add foreign key (HomeCountryID) references country(ID);

insert into books(Title, PublishDate, Price, AuthorID, BestSeller)
VALUES('Introduction to algorithm','1990-5-30',120,2,true);
insert into books(Title, PublishDate, Price, AuthorID, BestSeller)
VALUES('The clean coder','2000-6-30',200,2,true);
insert into books(Title, PublishDate, Price, AuthorID, BestSeller)
VALUES('Design pattern: Elements of reusable object-oriented','2010-2-18',120,4,true);
insert into books(Title, PublishDate, Price, AuthorID, BestSeller)
VALUES('The pragmatic programmer','1999-5-30',450,5,true);
insert into books(Title, PublishDate, Price, AuthorID, BestSeller)
VALUES('The art of computer programming vol:1','2000-6-30',350,6,true);
insert into books(Title, PublishDate, Price, AuthorID, BestSeller)
VALUES('The art of computer programming vol:2','2000-6-30',350,6,true);
insert into books(Title, PublishDate, Price, AuthorID, BestSeller)
VALUES('The art of computer programming vol:3','2000-6-30',350,6,true);
insert into books(Title, PublishDate, Price, AuthorID, BestSeller)
VALUES('Lazy hackers','1999-5-30',20,7,false);

insert into author(RealID, BirthDate, HomeCountryID)
VALUES('152436987','1970-11-25',1);
insert into author(RealID, BirthDate, HomeCountryID)
VALUES('542178396','1970-11-25',3);
insert into author(RealID, BirthDate, HomeCountryID)
VALUES('200139568','1970-11-25',2);
insert into author(RealID, BirthDate, HomeCountryID)
VALUES('053689742','1970-11-25',1);
insert into author(RealID, BirthDate, HomeCountryID)
VALUES('985620030','1960-2-15',2);

insert into country(Code, Name)
VALUES('USA','United-Stats');
insert into country(Code, Name)
VALUES('CAN','Canada');
insert into country(Code, Name)
VALUES('JAP','Japan');

--WE ASKED TO CREATE STORED PROCEDURES (QUS' 3.3)  ABOUT AUTHORS NAME, SO IM ADDING NEW COLUMN TO AUTHORS TABLE
 ALTER TABLE author ADD COLUMN Name text;

--Creating Views
create view all_books_data AS
SELECT b.Title,b.Price,b.PublishDate,c.Code FROM books b JOIN author a ON b.Authorid = a.ID
JOIN country c ON a.HomeCountryID= c.ID;

create view all_authors AS
SELECT a.RealID, a.BirthDate,c.Name, c.Code FROM author a JOIN country c ON a.HomeCountryID= c.ID;

create view authors_books AS
SELECT a.ID, Count(a.ID) books_published FROM author a JOIN books b ON a.ID= b.AuthorID
group by a.ID;

create view lastest_published_books AS
SELECT * FROM books
ORDER BY  PublishDate desc limit 1;

create view japanese_books AS
SELECT b.Title,b.Price,b.PublishDate,c.Code FROM books b JOIN author a ON b.Authorid = a.ID
JOIN country c ON a.HomeCountryID= c.ID
WHERE upper(c.Name) LIKE 'JAPAN' or upper(c.Code) LIKE 'JAP';

create view american_books AS
SELECT b.Title,b.Price,b.PublishDate,c.Code FROM books b JOIN author a ON b.Authorid = a.ID
JOIN country c ON a.HomeCountryID= c.ID
WHERE upper(c.Name) LIKE 'UNITED-STATS' or upper(c.Code) LIKE 'USA';

CREATE VIEW authors_best_seller AS
SELECT b.Title, b.Price FROM author a JOIN books b ON a.ID= b.AuthorID
WHERE b.bestseller = true;

--========STORED PROCEDURES=========
--3.1
CREATE FUNCTION sp_get_all_authors()
returns Table(RealID bigint, BirthDate date, Name text, Code text)
language plpgsql AS
    $$
    BEGIN
        return query
        SELECT a.RealID, a.BirthDate,c.Name, c.Code FROM author a JOIN country c ON a.HomeCountryID= c.ID;
    END;
    $$;

--3.2
CREATE FUNCTION sp_books_by_country(_country_code text)
returns Table(Title text, Price real, PublishDate date, Code text)
language plpgsql AS
    $$
    BEGIN
        return query
        SELECT b.Title,b.Price,b.PublishDate,c.Code FROM books b JOIN author a ON b.Authorid = a.ID
        JOIN country c ON a.HomeCountryID= c.ID
        WHERE upper(c.Code) LIKE _country_code;
    END;
    $$;

SELECT * FROM sp_books_by_country('USA');

--3.3
CREATE FUNCTION sp_books_by_authors(_author_name text)
returns table(Title text, Price real, Publish_Date date, author_Name text)
language plpgsql AS
    $$
    BEGIN
        return query
        SELECT b.Title,b.Price,b.PublishDate,a.Name FROM books b JOIN author a ON b.Authorid = a.ID
        WHERE a.Name LIKE _author_name;
    END;
    $$;

SELECT * FROM sp_books_by_authors('Tomas');

--3.4 CHALLENGE
CREATE FUNCTION sp_upset_country(_country_code text,_country_name text, new_id int)
returns table(country text)
language plpgsql AS
    $do$
    BEGIN
        IF (_country_name = country.Name) THEN
            UPDATE country
            SET country.Name = _country_name,
                country.Code = _country_code
            where country.ID = new_id;
        ELSE
            insert into country VALUES (_country_code, _country_name);
        end if;
    END;
    $do$;

SELECT * FROM sp_upset_country('CAN','Canada', 2)

